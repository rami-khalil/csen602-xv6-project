#include "types.h"
#include "x86.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "pstat.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;
  
  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// System call tracking code
int system_call_count = 0;
int
sys_getsyscallinfo(void)
{
  return system_call_count ? system_call_count : -1;
}

// Shutdown system call
int
sys_shutdown(void)
{
  cprintf("Shutdown signal sent\n");
  outw( 0xB004, 0x0 | 0x2000 );

  return 0;
}

// Lottery scheduler ticket setter
int
sys_settickets(void)
{
  int num;
  if(argint(0, &num) < 0)
    return -1;
  if(num < 0)
    return -1;

  cprintf("Setting tickets of PID %d to %d\n", proc->pid, num);
  proc->numtickets = num;
  return 0;
}

// Scheduler Statistics
struct pstat pstat;
int
sys_getpinfo(void)
{
  struct pstat *st;
  if(argptr(0, (void*)&st, sizeof(*st)) < 0 )
    return -1;
  //acquire(&ptable.lock);
  int i;
  for (i = 0; i < NPROC; i++)
    st->inuse[i] = pstat.inuse[i],
    st->pid[i] = pstat.pid[i],
    st->hticks[i] = pstat.hticks[i],
    st->lticks[i] = pstat.lticks[i];
  //release(&ptable.lock);
  return 0;
}

// Create kernel thread
int
sys_kthread_create(void) {
  void (*handler)();
  void  *stack;
  uint stack_size;
  if(argptr(0, (char**) &handler, sizeof(handler)) < 0)
    return -1;
  if(argptr(1, (char**) &stack, sizeof(stack)) < 0)
    return -1;
  if(argint(2, (int*) &stack_size) < 0)
    return -1;
  // TODO: do stuff
  return -1;
}
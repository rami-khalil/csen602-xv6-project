#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  printf(1, "Testing Ticketing Scheduler By Creating 4 new processes:\n");
  int i;
  for(i = 1; i <= 4; i++) {
  	int pid = fork();
  	if(pid == 0) {
  		settickets(3*i);
  		for(;;);
  	} else {
  		printf(1, "Child with PID %d and %d tickets created.\n", pid, 3*i);
  	}
  }
  exit();
}

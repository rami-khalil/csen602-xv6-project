#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  printf(1, "Process scheduling statistics:\n");
  printf(1, "Slot\tPID\tHigh\tLow\n");
  struct pstat st;
  getpinfo(&st);
  int i;
  for(i = 0; i < NPROC; i++)
  		if(st.inuse[i])
  			printf(1, "%d\t%d\t%d\t%d\n", i, st.pid[i], st.hticks[i], st.lticks[i]);
  exit();
}

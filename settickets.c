#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  printf(1, "I am a new process that will now get two tickets.\n");
  settickets(2);
  exit();
}

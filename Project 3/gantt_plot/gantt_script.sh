#!/usr/bin/gnuplot
reset
set terminal png

set xlabel "Time slice"

set ylabel "Process Running"

set title "Scheduler Timing"

plot "gantt_cpu0_numbered.dat" using 1:3 title "cpu0" with points pt 7 ps 0.3 lc rgb "red", \
	 "gantt_cpu1_numbered.dat" using 1:3 title "cpu1" with points pt 7 ps 0.3 lc rgb "blue"
